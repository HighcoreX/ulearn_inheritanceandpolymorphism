﻿using System;

namespace uLearn_InheritanceAndPolymorphism
{
    public struct Parameter
    {
        public readonly int baseValue;
        private int additiveModifier;
        private double multiplicativeModifier;
        private int currentValue;
        private void UpdateCurrentValue()
        {
            currentValue = Convert.ToInt32((baseValue + additiveModifier) * (1.0 + multiplicativeModifier));
        }
        public int AdditiveModifier 
        {
            get { return additiveModifier; }
            set
            {
                additiveModifier = value;
                UpdateCurrentValue();
            }
        }
        public double MultiplicativeModifier
        {
            get { return multiplicativeModifier; }
            set
            {
                multiplicativeModifier = value;
                UpdateCurrentValue();
            }
        }
        public int CurrentValue { get { return currentValue; } }
        public Parameter(int value)
        {
            baseValue = value;
            currentValue = value;
            additiveModifier = 0;
            multiplicativeModifier = 0.0;
        }
    }
}
