﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Reflection;

namespace uLearn_InheritanceAndPolymorphism
{
    public class Modifier : Timer
    {
        private ICharacter target;
        private ISkill skill;
        private void Modify<T>(KeyValuePair<string, T>[] skillModifiers, string propertyName, Func<T, T, T> operation)
        {
            PropertyInfo propertyInfo;
            object propertyValue;
            PropertyInfo Modifier;
            foreach (var mod in skillModifiers)
            {
                propertyInfo = target.GetType().GetProperty(mod.Key);
                if (propertyInfo == null) continue;
                propertyValue = propertyInfo.GetValue(target);
                Modifier = propertyValue.GetType().GetProperty(propertyName);
                Modifier.SetValue(propertyValue, operation((T)Modifier.GetValue(propertyValue), mod.Value));
                propertyInfo.SetValue(target, propertyValue);
            }
        }
        private void SelfDispose(Object source, ElapsedEventArgs e)
        {
            Modify<int>(skill.AdditiveModifiers, "AdditiveModifier", (x, y) => x - y);
            Modify<double>(skill.MultiplicativeModifiers, "MultiplicativeModifier", (x, y) => x - y);
            target.Modifiers.Remove(this);
            this.Dispose();
        }
        public ISkill Skill
        {
            get { return skill; }
        }
        public Modifier(ICharacter target, ISkill skill)
            : base(skill.Duration)
        {
            this.target = target;
            this.skill = skill;
            Modify<int>(this.skill.AdditiveModifiers, "AdditiveModifier", (x, y) => x + y);
            Modify<double>(this.skill.MultiplicativeModifiers, "MultiplicativeModifier", (x, y) => x + y);
            this.Elapsed += SelfDispose;
            this.Start();
        }
    }
}
