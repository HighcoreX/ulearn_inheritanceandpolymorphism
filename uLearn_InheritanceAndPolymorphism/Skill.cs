﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace uLearn_InheritanceAndPolymorphism
{
    public interface ISkill
    {
        double Duration { get; }
        bool TargetSelf { get; }
        int ManaCost { get; }
        double Cooldown { get; }
        KeyValuePair<string, int>[] AdditiveModifiers { get; }
        KeyValuePair<string, double>[] MultiplicativeModifiers { get; }
        Timer CooldownTimer { get; set; }
    }
    public static class ISkillExtensions
    {
        public static bool Use(this ISkill skill, ICharacter target, ICharacter caster)
        {
            if (skill.CooldownTimer != null || caster.Mana - skill.ManaCost < 0 || (!skill.TargetSelf & target == caster)) return false;
            target.Modifiers.Add(new Modifier(target, skill));
            caster.Mana -= skill.ManaCost;
            skill.CooldownTimer = new Timer(skill.Cooldown);
            skill.CooldownTimer.Elapsed += (Object source, ElapsedEventArgs e) => { skill.CooldownTimer.Dispose(); };
            skill.CooldownTimer.Start();
            return true;
        }
    }
}
