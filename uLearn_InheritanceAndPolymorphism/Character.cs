﻿using System.Collections.Generic;

namespace uLearn_InheritanceAndPolymorphism
{
    public interface ICharacter
    {
        Parameter MaxHealth { get; set; }
        Parameter HealthRegeneration { get; set; }
        Parameter MaxMana { get; set; }
        Parameter ManaRegeneration { get; set; }
        int Health { get; set; }
        int Mana { get; set; }
        List<Modifier> Modifiers { get; set; }
        List<ISkill> Skills { get; set; }
    }
}
